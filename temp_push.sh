#!/bin/sh
red='\e[1;31m%s\e[0m\n'
green='\e[1;32m%s\e[0m\n'
yellow='\e[1;33m%s\e[0m\n'

team_name=$1

# Load credentials from file
source credentials.conf
username=$USER
password=$PASS

# verify team
declare_team() {
  if [[ $team_name == idc ]]; then
    printf "\nIDC\n"
  elif [[ $team_name == core ]]; then
    printf "\nCore\n"
  elif [[ $team_name == gateway ]]; then
    printf "\nGateway\n"
  elif [[ $team_name == '' ]]; then
    printf "\nTo run script type 'sh temp_push sh {team_name}'\nFor team_name choose:\n-idc\n-core\n-gateway\n"
    exit 0
  else
    printf "\nTo run script type 'sh temp_push sh {team_name}'\nFor team_name choose:\n-idc\n-core\n-gateway\n"
    exit 0
  fi
}

push_tests() {
  # get and assign path
  rootPath=$PWD

  # replace login and password in config.yml file
  sed -i -e 's/${apif_pipeline_username}/'$username'/g' $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/config.yml
  sed -i -e 's/${apif_pipeline_password}/'$password'/g' $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/config.yml

  # run python script to push tests to API Fortress Platform
  chmod +x $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/push_tests_$team_name.sh
  $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/push_tests_$team_name.sh $rootPath

  printf "\nAll tests was pushed to the API Fortress Platform\n"
}

clean() {
  # revert changes in config.yml file
  sed -i -e 's/'$username'/${apif_pipeline_username}/g' $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/config.yml
  sed -i -e 's/'$password'/${apif_pipeline_password}/g' $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/config.yml
  # remove temp file
  rm -rf $rootPath/api-fortress-cli-tool/apif-auto-1.1.1/config.yml-e
}

main() {
  printf "$yellow" "This script push all API Fortress tests from Bitbucket to API Fortress Platform"
  declare_team "$team_name"
  push_tests "$team_name"
  clean
  printf "$red" "Please remember to commit all changes to the repository!"
}

main
